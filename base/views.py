from django.shortcuts import render

def index(request):
	template_name = 'base2.html'
	data = {}
	return render(request, template_name, data)

def index_inicio(request):
	template_name = 'base_inicio.html'
	data = {}
	return render(request, template_name, data)