from django.contrib import admin

from empresa.models import Empresa, Obra

@admin.register(Empresa)
class Empresa(admin.ModelAdmin):
	pass

@admin.register(Obra)
class Obra(admin.ModelAdmin):
	pass

