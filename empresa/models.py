from django.db import models

class Empresa(models.Model):
	name = models.CharField(max_length=144)
	rut = models.CharField(max_length=10)
	email = models.EmailField(max_length=254, null=True, blank=True)
	address_1 = models.CharField(max_length=144)	
	created_at = models.DateTimeField(auto_now_add=True)
	modified_at = models.DateTimeField(auto_now=True)

	def __str__(self):
		return self.name

class Obra(models.Model):
	name = models.CharField(max_length=144)
	location = models.CharField(max_length=144)
	number_obra = models.IntegerField()
	empresa = models.ForeignKey(Empresa, null=False, blank=False, on_delete=models.CASCADE)
	start_date = models.DateTimeField(auto_now_add=False)
	end_date = models.DateTimeField(auto_now_add=False)
	created_at = models.DateTimeField(auto_now_add=True)
	modified_at = models.DateTimeField(auto_now=True)

	def __str__(self):
		return self.name