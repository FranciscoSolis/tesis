from django.shortcuts import render
from empresa.models import Empresa


def home(request):
    template_name = 'empresa.html'
    data = {}
    # select * from empresa
    data['datos_empresa'] = Empresa.objects 

    return render(request, template_name, data)