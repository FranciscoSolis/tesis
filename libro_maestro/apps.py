from django.apps import AppConfig


class LibroMaestroConfig(AppConfig):
    name = 'libro_maestro'
